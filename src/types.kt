package ry

interface RenderableI {
    fun render(r: Renderer)
}