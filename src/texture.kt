package ry

import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL12.*
import org.lwjgl.opengl.GL13.*
import org.lwjgl.opengl.GL14.*
import org.lwjgl.opengl.GL15.*
import org.lwjgl.opengl.GL20.*
import org.lwjgl.opengl.GL21.*
import org.lwjgl.opengl.GL30.*
import org.lwjgl.system.MemoryUtil.*
import org.lwjgl.BufferUtils.*
import org.lwjgl.stb.STBImage.*
import org.lwjgl.stb.STBImageWrite.*

class Texture {
    var width = 0
    var height = 0
    // gl:
    var gl_tex = 0
    private var gl_mag_filter = 0
    private var gl_min_filter = 0
    private val t = this

    init {

    }

    constructor(width: Int, height: Int, depth: Boolean) {
        t.width = width
        t.height = height

        gl_tex = createTex(width, height, depth)

        TextureSys.addElem(t)
    }

    fun tex(): Int = this.gl_tex
    fun size(): Pair<Int, Int> = Pair(t.width, t.height)

    fun bind() {
        glActiveTexture(GL_TEXTURE0) // fixme: remove?
        glBindTexture(GL_TEXTURE_2D, t.gl_tex)
    }

    fun unbind() {
        glBindTexture(GL_TEXTURE_2D, 0)
    }

    fun save(path: String) {
        // fixme: doesn't work with depth textures
        println("Texture: Saving texture to path: $path")
        val (iw, ih) = t.size()
        //var raw_img []uint8 = make([]uint8, iw*ih*4)
        //var raw_img: IntArray = IntArray(iw*ih*4)
        val raw_img = BufferUtils.createByteBuffer(iw*ih*4)

        //gl.Disable(gl.BLEND)
        t.bind()
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, raw_img)
        t.unbind()
        //gl.Enable(gl.BLEND)
        glcheck()

        /*
        im := image.NewNRGBA(image.Rect(0, 0, iw, ih))
        im.Pix = raw_img
        // flipY
        im = imaging.FlipV(im)

        of, err := os.Create(path)
        defer of.Close()
        if err != nil {
            Log.Err("can't save image to path; path: ", path)
            panic(err)
        }
        // disable compression
        enc := &png.Encoder{CompressionLevel: -1}
        enc.Encode(of, im)
        */
        stbi_write_png(path, iw, ih, 4, raw_img, iw*4)
    }

}

fun CopyTexture(dst: Int, width: Int, height: Int) {
    // Copies pixels from active screen (GL_READ_BUFFER) to dst texture.
    // XXX not properly tested.

    // Get Cur tex
    val prevTex = glGetInteger(GL_TEXTURE_BINDING_2D)

    glBindTexture(GL_TEXTURE_2D, dst)
    glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, width, height)
    glBindTexture(GL_TEXTURE_2D, 0)

    if (prevTex != 0) {
        println("restoring prevTex")
        glBindTexture(GL_TEXTURE_2D, prevTex)
    }
}

fun CopyTexture2(dst: Int, x: Int, y: Int, width: Int, height: Int) {
    // Copies pixels from active screen (GL_READ_BUFFER) to dst texture.
    // XXX not properly tested.
    p("CopyTexture2", x, y, width, height)

    // Get Cur tex
    val prevTex = glGetInteger(GL_TEXTURE_BINDING_2D)

    glBindTexture(GL_TEXTURE_2D, dst)
    glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, x, y, width, height)
    glBindTexture(GL_TEXTURE_2D, 0)

    if (prevTex != 0) {
        println("restoring prevTex")
        glBindTexture(GL_TEXTURE_2D, prevTex)
    }
}