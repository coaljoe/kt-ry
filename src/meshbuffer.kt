package ry

import java.nio.*
//import org.lwjgl.*
//import org.lwjgl.glfw.*
import org.lwjgl.opengl.*
import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GLCapabilities
import org.lwjgl.opengl.GLUtil
import org.lwjgl.system.Callback
//import org.lwjgl.system.*
//import java.time.Clock.system
//import org.lwjgl.glfw.Callbacks.*
//import org.lwjgl.glfw.GLFW.*
//import org.lwjgl.opengl.GL.*
import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL13.*
import org.lwjgl.opengl.GL15.*
import org.lwjgl.opengl.GL20.*
//import org.lwjgl.system.MemoryStack.*
import org.lwjgl.system.MemoryUtil.*
import org.lwjgl.BufferUtils.*
import glm.mat.Mat4
import glm.vec._3.Vec3
import glm.*

class MeshBuffer {
	//var fromRawData: bool
	var positions: FloatArray
	var normals: FloatArray
	var texcoords: FloatArray
	//var indices: seq[uint32]
	var numPositions: Int
	var positionsRaw: ByteArray
	var normalsRaw: ByteArray
	var texcoordsRaw: ByteArray
	var hasPositions: Boolean
	var hasNormals: Boolean
	var hasTexCoords: Boolean
	var hasIndices: Boolean
	// gl
	private var gl_vBuf: Int = 0
	private var gl_nBuf: Int = 0
	private var gl_tBuf: Int = 0
	private var gl_iBuf: Int = 0
	private val mb = this
	
	init {
	  //fromRawData = true
	  positions = floatArrayOf()
	  normals = floatArrayOf()
	  texcoords = floatArrayOf()
	  numPositions = 0
	  positionsRaw = byteArrayOf()
	  normalsRaw = byteArrayOf()
	  texcoordsRaw = byteArrayOf()
	  hasPositions = false
	  hasNormals = false
	  hasTexCoords = false
	  hasIndices = false 
	}
	
	fun build() {
	  // Check before for previous errrors
	  glcheck()
	
	  val sizeGLfloat: Int = 4
	  val sizeGLuint: Int = 4
	  //let sizeGLfloat = sizeof(GLfloat)
	
	  //if (mb.positionsRaw.size > 0) {
	  if (true) {
	    mb.hasPositions = true
	    mb.gl_vBuf = glGenBuffers()
	    glcheck()
	    glBindBuffer(GL_ARRAY_BUFFER, mb.gl_vBuf)
	    glcheck()
	    //mb.numPositions = mb.positionsRaw.size / sizeGLfloat
	    mb.numPositions = mb.positions.size
	    //val buf = ByteBuffer.allocate(mb.positionsRaw.length)
	    //buf.put(mb.positionsRaw.toByteArray())
	    //val buf = bytes_to_bb(mb.positionsRaw)
	    //buf.rewind()
	    //buf.flip()
	    
	    /*
	    //val arr = mb.positionsRaw
	    //val buf = createByteBuffer(arr.size)
	    //buf.put(arr)
	    ///
	    val buf = createByteBuffer(mb.positionsRaw.size)
	    buf.put(mb.positionsRaw)
	    buf.flip()
	    //buf.rewind()
	    ///
	    //val mb.positionsRaw.rewind()
	    
	    
	    for (i in 0 until 18) {
	        println("-> %f".format(buf.getFloat()))
	        if ((i % 3) == 0)
	            println()
	    }
	    */
	    
	    
	    //buf.flip()
	    /*
	    println(buf.size)
	    for (i in 0..15) {
	        println("-> %f".format(buf.getFloat()))
	    }
	    println("...")
	    error(2)
	    */
	    //glBufferData(GL_ARRAY_BUFFER, buf, GL_STATIC_DRAW)
	    
	    //val buf2 = buf.asFloatBuffer()
	    //buf2.flip()
	    //glBufferData(GL_ARRAY_BUFFER, buf2, GL_STATIC_DRAW)
	    
	    
	    /*
	    val buf1 = floatArrayOf(
	    1.0f, -0.0f, -1.0f,
	    -1.0f, 0.0f, 1.0f,
	    -1.0f, -0.0f, -1.0f,
	    1.0f, -0.0f, -1.0f,
	    1.0f, 0.0f, 1.0f,
	    -1.0f, 0.0f, 1.0f)
	    
	    glBufferData(GL_ARRAY_BUFFER, buf1, GL_STATIC_DRAW)
	    */

        val xbuf = mb.positions
        glBufferData(GL_ARRAY_BUFFER, xbuf, GL_STATIC_DRAW)
        	    	    
	    
	    glcheck()
	    //quit -1
	  }
	
	  if (mb.normalsRaw.size > 0) {
	    mb.hasNormals = true
	    mb.gl_nBuf = glGenBuffers()
	    val buf = bytes_to_bb(mb.normalsRaw)
	    glBindBuffer(GL_ARRAY_BUFFER, mb.gl_nBuf)
	    glBufferData(GL_ARRAY_BUFFER, buf, GL_STATIC_DRAW)
	  }
	  if (texcoordsRaw.size > 0) {
	    mb.hasTexCoords = true
	    mb.gl_tBuf = glGenBuffers()
	    val buf = bytes_to_bb(mb.texcoordsRaw)
	    glBindBuffer(GL_ARRAY_BUFFER, mb.gl_tBuf)
	    glBufferData(GL_ARRAY_BUFFER, buf, GL_STATIC_DRAW)
	  }
	  /*
	  if (len(mb.indices) > 0) {
	    mb.hasIndices = true
	    mb.gl_iBuf = glGenBuffers()
	    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mb.gl_iBuf)
	    //glBufferData(GL_ELEMENT_ARRAY_BUFFER, GLsizeiptr(len(mb.indices)), mb.indices[0], GL_STATIC_DRAW)
	    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mb.indices*sizeGLuint, mb.indices[0], GL_STATIC_DRAW)
	  }
	  */ 
	
	  glcheck()
	
	  // Unbind buffers
	  glBindBuffer(GL_ARRAY_BUFFER, 0)
	  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
	}
	
	fun render() {
	  if (!mb.hasPositions)
	    error("no positions")
	  
	  glEnableClientState(GL_VERTEX_ARRAY)
	  if (mb.hasTexCoords)
	    glEnableClientState(GL_TEXTURE_COORD_ARRAY)
	
	  glBindBuffer(GL_ARRAY_BUFFER, mb.gl_vBuf)
	  glVertexPointer(3, GL_FLOAT, 0, NULL)
	  //glVertexPointer(3, GL_FLOAT, sizeofGLfloat, mb.gl_vBuf)
	  //glVertexPointer(3, GL_FLOAT, 0, mb.positions)
	
	  if (mb.hasTexCoords) {
	    glClientActiveTexture(GL_TEXTURE0)
	    glBindBuffer(GL_ARRAY_BUFFER, mb.gl_tBuf)
	    glTexCoordPointer(2, GL_FLOAT, 0, NULL) // UV
	    //glTexCoordPointer(3, GL_FLOAT, 0.GLsizei, null) // UVW
	   }
	
	  if (mb.hasIndices) {
	    error("unsupported rendering mode")
	  } else {
	    glDrawArrays(GL_TRIANGLES, 0, mb.numPositions/3)
	    //glDrawArrays(GL_TRIANGLES, int32(0), int32(len(mb.positions)/3))
	    //gl.DrawArrays(gl.TRIANGLES, int32(0), 1)
	    //gl.DrawArrays(gl.TRIANGLE_STRIP, int32(0), int32(len(mb.positions)/4))
	  }
	
	  glDisableClientState(GL_VERTEX_ARRAY)
	  if (mb.hasTexCoords)
	    glDisableClientState(GL_TEXTURE_COORD_ARRAY)
	
	  glcheck()
	
	  glBindBuffer(GL_ARRAY_BUFFER, 0)
	  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
	}
		
	// Create MeshBuffer from raw data	
	fun addRawData(positions: ByteArray, normals: ByteArray, texcoords: ByteArray) {
	  mb.positionsRaw = positions
	  mb.normalsRaw = normals
	  mb.texcoordsRaw = texcoords
	  mb.build()
	}	
	
	// Create MeshBuffer from data
	fun addData(positions: FloatArray, normals: FloatArray, texcoords: FloatArray) {
	    mb.positions = positions
	    mb.normals = normals
	    mb.texcoords = texcoords
	    mb.build()
	}
	
}

private fun bytes_to_bb(arr: ByteArray): ByteBuffer {
    //val buf = ByteBuffer.allocate(arr.size)
    val buf = ByteBuffer.allocateDirect(arr.size)
    buf.put(arr)
    return buf
}

private fun string_to_bb(s: String): ByteBuffer {
	val buf = ByteBuffer.allocate(s.length)
	buf.put(s.toByteArray())
	return buf
}