package ry

//import com.jogamp.opengl.GL2
//import com.jogamp.opengl.*
//import com.jogamp.opengl.GL2ES2.GL_STREAM_DRAW
//import com.jogamp.opengl.GL2ES3.*
//val gl = GL2

import com.sun.org.apache.bcel.internal.generic.FLOAD
import org.lwjgl.*
import org.lwjgl.glfw.*
import org.lwjgl.opengl.*
import org.lwjgl.system.*

import java.nio.*

import java.time.Clock.system
import org.lwjgl.glfw.Callbacks.*
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.opengl.GL11.*
import org.lwjgl.system.MemoryStack.*
import org.lwjgl.system.MemoryUtil.*

import glm.mat.Mat4
import glm.vec._3.Vec3
import glm.*
//import sun.plugin2.os.windows.FLASHWINFO

object Renderer {
    var width = 0
    var height = 0
    // Current camera
    //var cam: Camera? = null
    var cam: Camera = Scene.camera.camera!!
    //var scene: Scene
    //var renderPasses
    //var renderables: LinkedHashMap<Int, Node>
    //var renderables: MutableSet<RenderableI> = mutableSetOf()
    var renderables: MutableList<RenderableI> = mutableListOf()
    var tmpRt: RenderTarget? = null
    var in2dmode: Boolean = false
    var trenderer: TRenderer? = null
    var debug = true
    private val r = this

    fun init(width: Int, height: Int) {
    	println("[Renderer] init $Renderer")
        r.width = width
        r.height = height
        r.tmpRt = RenderTarget("tmpRt", r.width, r.height, false)

        //glClearColor(0.0, 0.0, 0.0, 1.0)
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f) // XXX warning, transparent background
        glClearDepth(1.0)
        glColor3f(1.0f, 1.0f, 1.0f)
        glEnable(GL_DEPTH_TEST)
        glDepthFunc(GL_LEQUAL)

        glEnable(GL_TEXTURE_2D)
        //glEnable(GL_CULL_FACE)

        // Set blend func
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        //glEnable(GL_BLEND) // XXX should be disabled by default

        // Set viewport
        glViewport(0, 0, width, height)

        // Default projection
        if (false) {
            // set projection matrix
            glMatrixMode(GL_PROJECTION)
            glLoadIdentity()

            // Set perspective by default XXX fixme : remove
            // Enable perspective projection with fovy, aspect, zNear and zFar
            //gluPerspective(45.0, width.toFloat() / height.toFloat(), 0.1, 100.0)
        }

        // Set modelview matrix
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        // Set default camera
        cam = Scene.camera.camera!!
        println("{$r.cam} ${r.cam == null} $Scene.camera")
        println("${Scene}")
        //error(2)

        // Init TRenderer
        //trenderer = TRenderer(2, 2)
    }

    fun getPw(): Float = 1.0f / r.width
    fun getPh(): Float = 1.0f / r.height
    fun getAspect(): Float = r.width.toFloat() / r.height.toFloat()

    fun setCamera(c: Camera) {
		//val cam = n.get(Camera)
		val cam = c
    	// Set projection matrix
    	glMatrixMode(GL_PROJECTION)
    	glLoadIdentity()
    	
    	var projM = Mat4(cam.getProjectionMatrix())
    	//println("projM")
    	//print_matrix(projM)
    	// Multiply by projection matrix
    	xglMultMatrix(projM) 
    	
    	val viewM = Mat4(cam.getViewMatrix())
    	//println("viewM")
    	//print_matrix(viewM)   	
    	// Multiply by view matrix
    	xglMultMatrix(viewM)
    	
    	// Set model matrix
    	glMatrixMode(GL_MODELVIEW)
    	glLoadIdentity()
    }

    fun renderQuad(x: Float, y: Float, w: Float, h: Float, z: Float) {
        if (debug) {
            if (w > 1.0 || h > 1.0)
                error("w or h is too big (must be <= 1.0)")
        }

        // Prepare
        //glPushAttrib(GL_CURRENT_BIT | GL_TEXTURE_BIT | GL_DEPTH_TEST)
        glPushAttrib(GL_CURRENT_BIT or GL_TEXTURE_BIT or GL_DEPTH_TEST)
        //glPushAttrib(GL_CURRENT_BIT) // XXX: not tested
        glDisable(GL_DEPTH_TEST)
        glColor3f(1f, 1f, 1f) // important

        glMatrixMode(GL_PROJECTION)
        glPushMatrix()
        glLoadIdentity()
        glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0) // normal coords
        ////gl.Ortho(0, 1, 1, 0, -1, 1) // Y-flipped
        glMatrixMode(GL_MODELVIEW)
        glPushMatrix()
        glLoadIdentity()

        ////gl.Translatef(0, 0, float32(z)) // ok
        ////gl.Translatef(0, float32(z), 0) // xyz upd: check [bad]

        glBegin(GL_QUADS)
        glTexCoord2f(0f, 0f)
        glVertex2f(x, y)
        glTexCoord2f(1f, 0f)
        glVertex2f(x+w, y)
        glTexCoord2f(1f, 1f)
        glVertex2f(x+w, y+h)
        glTexCoord2f(0f, 1f)
        glVertex2f(x, y+h)
        glEnd()

        // Restore
        glMatrixMode(GL_PROJECTION)
        glPopMatrix()
        glMatrixMode(GL_MODELVIEW)
        glPopMatrix()

        glPopAttrib()
    }

    fun renderFsQuad() {
        // From:
        // https://www.opengl.org/wiki/Viewing_and_Transformations#How_do_I_draw_a_full-screen_quad.3F

        // Prepare
        glPushAttrib(GL_DEPTH_TEST)
        glDisable(GL_DEPTH_TEST)
        glColor3f(1.0f, 1.0f, 1.0f) // important

        val z: Float = -1.0f

        glMatrixMode(GL_MODELVIEW)
        glPushMatrix()
        glLoadIdentity()
        glMatrixMode(GL_PROJECTION)
        glPushMatrix()
        glLoadIdentity()
        glBegin(GL_QUADS)
        glTexCoord2i(0, 0)
        glVertex3f(-1f, -1f, z)
        glTexCoord2i(1, 0)
        glVertex3f(1f, -1f, z)
        glTexCoord2i(1, 1)
        glVertex3f(1f, 1f, z)
        glTexCoord2i(0, 1)
        glVertex3f(-1f, 1f, z)
        glEnd()
        glPopMatrix()
        glMatrixMode(GL_MODELVIEW)
        glPopMatrix()

        glPopAttrib()
    }

    // Enter 2D mode
    fun set2DMode() {
        if (r.in2dmode) {
            return
        }

        // Prepare
        glPushAttrib(GL_CURRENT_BIT or GL_TEXTURE_BIT or GL_DEPTH_TEST)
        //gl.Disable(gl.DEPTH_TEST)
        //glDisable(gl.LIGHTING)
        glColor3f(1f, 1f, 1f) // important

        glMatrixMode(GL_PROJECTION)
        glPushMatrix()
        glLoadIdentity()
        glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0) // normal coords
        //gl.Ortho(0, 1, 1, 0, -1, 1) // Y-flipped
        glMatrixMode(GL_MODELVIEW)
        glPushMatrix()
        glLoadIdentity()

        r.in2dmode = true
    }

    // Exit 2D mode
    fun unset2DMode() {
        if (!r.in2dmode) {
            return
        }
        //r.SetCamera(r.curCam)

        // Restore
        glMatrixMode(GL_PROJECTION)
        glPopMatrix()
        glMatrixMode(GL_MODELVIEW)
        glPopMatrix()

        glPopAttrib()

        r.in2dmode = false
    }

    fun clear() {
        glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
    }

    fun finalize() {
        glBindTexture(GL_TEXTURE_2D, 0)
    }

    fun enableBlending() {
        glEnable(GL_BLEND)
    }

    fun disableBlending() {
        glDisable(GL_BLEND)
    }

    fun reset() {
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        //glColor3f(0.0, 1.0, 0.0) // Green
    }

    fun renderScene() {
        for (n in Scene.getNodes()) {
            if (!n.visible)
                continue
            
            if (n.mesh != null) {
                r.renderMesh(n)
            }
        }
    }
    
    fun renderMesh(n: Node) {
        //println("Renderer: renderMesh")
        val m = n.mesh!!
        
        glPushMatrix()
        val mat = n.transform.mat
        xglMultMatrix(mat)
        m.mb.render()
        glPopMatrix()
    }

    fun renderRenderables() {

        for (ri in r.renderables.toTypedArray()) {
            //discard ri.render(r)
            r.renderRenderable(ri)
        }

    /*
        var sortedRenderables = mutableListOf<RenderableI>()
        var sortedCards = mutableListOf<RenderableI>()
        var sortedSpriteMeshes = mutableListOf<RenderableI>()
        for (ri in r.renderables.toTypedArray()) {
            if (ri is Card) {
                sortedCards.add(ri)
            }
            if (ri is SpriteMesh) {
                sortedSpriteMeshes.add(ri)
            }
        }

        sortedSpriteMeshes.sortBy { ri ->
            -(ri as SpriteMesh).node().transform.pos.y
            //-(ri as SpriteMesh).node().transform.pos.x
        }
        sortedCards.clear()
        for (ri in sortedSpriteMeshes) {
            sortedCards.add((ri as SpriteMesh).card)
        }

        sortedRenderables.addAll(sortedCards)
        sortedRenderables.addAll(sortedSpriteMeshes)

        for (ri in sortedRenderables.toTypedArray()) {
            //discard ri.render(r)
            r.renderRenderable(ri)
        }
    */
    }

    fun renderRenderable(ri: RenderableI) {
        ri.render(r)
    }

    fun render() {
        r.tmpRt!!.bind()

        r.clear()
        r.reset()
        //glMatrixMode(GL_MODELVIEW)
        //glLoadIdentity()
        //r.cam.get(Transform).pos = vec3f(1.5, 0.0, -10)
        //r.cam.get(Transform).pos = vec3f(0, 0.0, 500)
        r.setCamera(r.cam!!)
        //glTranslatef(1.5, 0.0, -10.0)
        //glTranslatef(1.5, 0.0, -1000.0)
        glColor3f(0.0f, 1.0f, 0.0f)     // Green
       

        if (false) {
            if (false) {
            glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT) // Clear color and depth buffers
            glMatrixMode(GL_MODELVIEW)                          // To operate on model-view matrix
            glLoadIdentity()                 // Reset the model-view matrix
            //glTranslatef(1.5f, 0.0f, -7.0f)     // Move right and into the screen
            //glScalef(0.1f, 0.1f, 0.1f)
            }

			//glScalef(0.1f, 0.1f, 0.1f)
            glPushMatrix()
            //glScalef(0.1f, 0.1f, 0.1f)
        
            // Render a cube consisting of 6 quads
            // Each quad consists of 2 triangles
            // Each triangle consists of 3 vertices
        
            glBegin(GL_TRIANGLES)        // Begin drawing of triangles
        
            // Top face (y = 1.0f)
            glColor3f(0.0f, 1.0f, 0.0f)     // Green
            glVertex3f( 1.0f, 1.0f, -1.0f)
            glVertex3f(-1.0f, 1.0f, -1.0f)
            glVertex3f(-1.0f, 1.0f,  1.0f)
            glVertex3f( 1.0f, 1.0f,  1.0f)
            glVertex3f( 1.0f, 1.0f, -1.0f)
            glVertex3f(-1.0f, 1.0f,  1.0f)
        
            // Bottom face (y = -1.0f)
            glColor3f(1.0f, 0.5f, 0.0f)     // Orange
            glVertex3f( 1.0f, -1.0f,  1.0f)
            glVertex3f(-1.0f, -1.0f,  1.0f)
            glVertex3f(-1.0f, -1.0f, -1.0f)
            glVertex3f( 1.0f, -1.0f, -1.0f)
            glVertex3f( 1.0f, -1.0f,  1.0f)
            glVertex3f(-1.0f, -1.0f, -1.0f)
        
            // Front face  (z = 1.0f)
            glColor3f(1.0f, 0.0f, 0.0f)     // Red
            glVertex3f( 1.0f,  1.0f, 1.0f)
            glVertex3f(-1.0f,  1.0f, 1.0f)
            glVertex3f(-1.0f, -1.0f, 1.0f)
            glVertex3f( 1.0f, -1.0f, 1.0f)
            glVertex3f( 1.0f,  1.0f, 1.0f)
            glVertex3f(-1.0f, -1.0f, 1.0f)
        
            // Back face (z = -1.0f)
            glColor3f(1.0f, 1.0f, 0.0f)     // Yellow
            glVertex3f( 1.0f, -1.0f, -1.0f)
            glVertex3f(-1.0f, -1.0f, -1.0f)
            glVertex3f(-1.0f,  1.0f, -1.0f)
            glVertex3f( 1.0f,  1.0f, -1.0f)
            glVertex3f( 1.0f, -1.0f, -1.0f)
            glVertex3f(-1.0f,  1.0f, -1.0f)
        
            // Left face (x = -1.0f)
            glColor3f(0.0f, 0.0f, 1.0f)     // Blue
            glVertex3f(-1.0f,  1.0f,  1.0f)
            glVertex3f(-1.0f,  1.0f, -1.0f)
            glVertex3f(-1.0f, -1.0f, -1.0f)
            glVertex3f(-1.0f, -1.0f,  1.0f)
            glVertex3f(-1.0f,  1.0f,  1.0f)
            glVertex3f(-1.0f, -1.0f, -1.0f)
        
            // Right face (x = 1.0f)
            glColor3f(1.0f, 0.0f, 1.0f)    // Magenta
            glVertex3f(1.0f,  1.0f, -1.0f)
            glVertex3f(1.0f,  1.0f,  1.0f)
            glVertex3f(1.0f, -1.0f,  1.0f)
            glVertex3f(1.0f, -1.0f, -1.0f)
            glVertex3f(1.0f,  1.0f, -1.0f)
            glVertex3f(1.0f, -1.0f,  1.0f)
        
            glEnd()  // End of drawing
            
            glPopMatrix()
        }

		//val _cam = r.cam
		//var pos: Vec3 = _cam.entity!!.get<Transform>().pos
        //println("pos = ${pos.x} ${pos.y} ${pos.z}")
        

        if (r.debug) {
            drawGrid(10f, 1f)
        }

        r.renderScene()
        r.renderRenderables()

        // Render passes
        r.trenderer?.render(r)

        r.tmpRt!!.unbind()


        // Render RT's texture on a fullscreen quad
        glBindTexture(GL_TEXTURE_2D, r.tmpRt!!.tex())
        r.renderFsQuad()
        glBindTexture(GL_TEXTURE_2D, 0)
        
        glcheck()
        r.finalize()
    }
}