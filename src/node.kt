package ry

var _max_node_id = -1

class Node {
    var id: Int
    var visible: Boolean
    var parent: Node?
    var children: MutableList<Node>
    // Links
    var transform: Transform
    var mesh: Mesh?
    var camera: Camera?
    var spriteMesh: SpriteMesh?
    private val n = this
    
    init {
        _max_node_id++
        id = _max_node_id
        visible = true
        parent = null
        children = mutableListOf<Node>()
        transform = Transform()
        mesh = null
        camera = null
        spriteMesh = null
    }

    override fun toString(): String {
        //if (this.id == 999) { error(2)}
        //println(visible)
        //println(this.visible)
        return "Node<id=${this.id}>"
    }
}

fun newEmptyNode(): Node {
    val n = Node()
    //n.addComponent(Transform())
    // Update links, fixme?
    //n.transform = n.get<Transform>()
    return n
}

fun newMeshNode(): Node {
    val n = newEmptyNode()
    //n.addComponent(Mesh())
    n.mesh = Mesh(n)
    return n
}

fun newCameraNode(): Node {
	val n = newEmptyNode()
	//n.addComponent(Camera())
    n.camera = Camera(n)
	return n
}

fun newSpriteMeshNode(): Node {
    val n = newEmptyNode()
    val x = SpriteMesh(n)
    //n.addComponent(x)
    n.spriteMesh = x
    //gSpriteMeshSys.xadd((Node)x.entity)
    return n
}
