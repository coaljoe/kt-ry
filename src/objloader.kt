package ry

import java.nio.*
import java.nio.file.*
import glm.*
import glm.vec._2.Vec2
import glm.vec._3.Vec3

class ObjLoader {
	val positions: MutableList<Float>
	val normals: MutableList<Float>
	val texcoords: MutableList<Float> // UVs
	// Indices
	val positionIndices: MutableList<Int> // Vertices
	val normalIndices: MutableList<Int>
	val texcoordIndices: MutableList<Int>
	// Out
	//val out_positions: MutableList<Vec3>
	//val out_normals: MutableList<Vec3>
	//val out_texcoords: MutableList<Vec2>
	//val out_positions: ArrayList<FloatArray>
	//val out_normals: ArrayList<FloatArray>
	//val out_texcoords: ArrayList<FloatArray>
	val out_positions: ArrayList<Float>
	val out_normals: ArrayList<Float>
	val out_texcoords: ArrayList<Float>	
	private val ol = this
	
	init {
		//positions = mutableListOf()
		//normals = mutableListOf()
		//texcoords = mutableListOf()
		positions = arrayListOf()
		normals = arrayListOf()
		texcoords = arrayListOf()
		positionIndices = mutableListOf()
		normalIndices = mutableListOf()
		texcoordIndices = mutableListOf()
		out_positions = arrayListOf()
		out_normals = arrayListOf()
		out_texcoords = arrayListOf()
	}

	fun load(path: String) {
		val lines = Files.readAllLines(Paths.get(path))
		for (line in lines) {
			//val tokens = line.split("\\s+")
			val tokens = line.split(" ")
			//println(line)
			//println(tokens)
			when (tokens[0]) {
				"v" -> {
					positions.add(tokens[1].toFloat())
					positions.add(tokens[2].toFloat())
					positions.add(tokens[3].toFloat())
				}
				"vn" -> {
					normals.add(tokens[1].toFloat())
					normals.add(tokens[2].toFloat())
					normals.add(tokens[3].toFloat())
				}
				"vt" -> {
					texcoords.add(tokens[1].toFloat())
					texcoords.add(tokens[2].toFloat())
					//texcoords.add(tokens[3].toFloat())
					//texcoords.add(0.0f)
				}
				"f"	-> {
					val t1 = tokens[1].split("/")
					// In this order V,T,N
					positionIndices.add(t1[0].toInt())
					if (t1[1] != "") {
						texcoordIndices.add(t1[1].toInt())
					}
					try {
    					normalIndices.add(t1[2].toInt())
    				} catch (e: IndexOutOfBoundsException) { null }
					
					val t2 = tokens[2].split("/")
					positionIndices.add(t2[0].toInt())
					if (t2[1] != "") {
						texcoordIndices.add(t2[1].toInt())
					}
					try {
    					normalIndices.add(t2[2].toInt())
    				} catch (e: IndexOutOfBoundsException) { null }
					
					val t3 = tokens[3].split("/")
					positionIndices.add(t3[0].toInt())
					if (t3[1] != "") {
						texcoordIndices.add(t3[1].toInt())
					}
					try {
    					normalIndices.add(t3[2].toInt())
       				} catch (e: IndexOutOfBoundsException) { null }
				}
				else -> null
			}
		}
		println("read complete:")
		showStats()
		println()
		showData()
		println("after unrolling indices:")
		unrollIndices()
		showStats()
		
		println()
		showData()
	}

	fun showStats() {
		println("- positions: ${positions.size}")
		println("- normals: ${normals.size}")
		println("- texcoords: ${texcoords.size}")
		println()
		println("- positionIndices: ${positionIndices.size}")
		println("- normalIndices: ${normalIndices.size}")
		println("- texcoordIndices: ${texcoordIndices.size}")
		println()
		println("- out_positions: ${out_positions.size}")
		println("- out_normals: ${out_normals.size}")
		println("- out_texcoords: ${out_texcoords.size}")
	}
	
	fun showData() {
	    println("positionIndices: ${positionIndices.toTypedArray().asList()}")
	    println("normalIndices: ${normalIndices.toTypedArray().asList()}")
	    println("texcoordIndices: ${texcoordIndices.toTypedArray().asList()}")
	    println("out_positions: ${out_positions.toTypedArray().asList()}")
	    /*
	    for (v in out_positions) {
	        println(v.asList())
	    }
	    */
	}

	fun unrollIndices() {
        // Unrolling code from here:
	    // https://github.com/opengl-tutorials/ogl/blob/master/common/objloader.cpp
		for (i in 0 .. ol.positionIndices.size - 1) {
		    println("unrolling f number ${i}")
			val pi = (ol.positionIndices[i]-1) * 3

			//println("pi=${pi} tci=${tci}")
			println("pi=${pi}")
			val pos_x = ol.positions[pi]
			val pos_y = ol.positions[pi+1]
			val pos_z = ol.positions[pi+2]

			//ol.out_positions.add(Vec3(pos_x, pos_y, pos_z))
			//ol.out_texcoords.add(Vec2(tc_u, tc_v))
			//ol.out_positions.add(floatArrayOf(pos_x, pos_y, pos_z))
			//ol.out_texcoords.add(floatArrayOf(tc_u, tc_v))
			ol.out_positions.addAll(arrayOf(pos_x, pos_y, pos_z))

			if (ol.texcoordIndices.size > 0) {
				val tci = (ol.texcoordIndices[i] - 1) * 2
				println("tci=${tci}")
				val tc_u = ol.texcoords[tci]
				val tc_v = ol.texcoords[tci + 1]
				val tc_w = 0.0
				ol.out_texcoords.addAll(arrayOf(tc_u, tc_v))
			}
			
		}
	}
	
	//fun toRaw(arr: ArrayList<FloatArray>): ByteArray {
	fun toRaw(arr: ArrayList<Float>): ByteArray {
	    val buf = ByteBuffer.allocate(arr.size * 4)
	    for (v in arr) {
	        buf.putFloat(v)
	    }
	    
	    //val xarr = mutableListOf<Float>()
	    /*
	    for (v in arr) {
	        for (i in 0..v.size-1) {
    	        //buf.putFloat(v[i])
    	        xarr.add(v[i])
	        }
	    }
	    val buf = ByteBuffer.allocate(xarr.size * 4)
	    for (v in xarr) {
    	    buf.putFloat(v)
	    }
	    */
	    return buf.array()
	}
}