package ry

import glm.vec._2.Vec2
import glm.vec._3.Vec3

class SpriteMesh(node_: Node): RenderableI {
    //mesh*: Mesh
    //node*: Node
    var node: Node
    val dimX: Int
    val dimY: Int
    var card: Card
    var texR: Texture  // Render texture
    //var texN: Texture  // Normal texture
    //var dim: Vec2
    var dirty: Boolean
    private val sm = this

    init {
        node = node_
        //texN = Texture()
        dimX = 512/8
        dimY = 512/8
        //val w = vars.resX-300
        //val h = vars.resY
        val w = dimX
        val h = dimY
        //w = vars.maxSpriteMeshSizeX
        //h = vars.maxSpriteMeshSizeY
        card = Card()
        //texR = CreateTexture(w, h, false),
        texR = Texture(w, h, false)

        dirty = true

        //sm.card.setDim(0f, 0f, 0.5f / Renderer.getAspect(), 0.5f)
        //sm.card.setDim(0f, 0f, 0.5f , 0.5f * Renderer.getAspect())
        val cardW = dimX * Renderer.getPw()
        sm.card.setDim(0f, 0f, cardW , cardW * Renderer.getAspect())
        sm.card.tex = sm.texR
        //sm.card.setDim(0, 0, 1.0, 1.0)
        //sm.card.tex = sm.rt.fb.tex
        //sm.node.addComponent(sm.mesh)
        //sm.node.add(sm.card)
        // Automatically add to the system
        //let x = sm.entity
        SpriteMeshSys.add(sm)
    }
    /*
    fun node(): Node {
        return ecs.getEntity(sm) as Node
    }
    */

    // Get sm's render target.
    fun rt(): RenderTarget {
        return SpriteMeshSys.rt!!
    }

    fun load(path: String) {
        val sl = SceneLoader()
        val newNode = sl.load(path)[0]
        //
        println(newNode)
        /*
        //quit 2
        //sm.node = newNode
        //sm.entity = newNode
        var entity = ecs.getEntity(sm)
        //entity = newNode
        //ecs.setEntity(this, entity)
        //entity.addComponent(this)
        entity.addComponent(newNode.mesh!!)
        */

        sm.node.mesh = newNode.mesh
    }

    //proc spawn*(sm: SpriteMesh, path: string) =
    //ryi.scene.add(sm.node)
    //echo ryi

    private fun build() {}

    fun onCameraMove(adjX: Float, adjY: Float) {
        updateRect()
    }

    override fun render(r: Renderer) {
        // XXX FIXME
        // call update here since spritemeshsys's update isn't working
        //sm.update(0)

        // Update once
        if (!sm.dirty)
            return

        println("sm render")

        //let w = sm.rt.fb.tex.width
        //let h = sm.rt.fb.tex.height
        //val w = sm.rt().fb.width
        //val h = sm.rt().fb.height
        val w = sm.dimX
        val h = sm.dimY


        r.finalize()

        sm.rt().bind()

        r.clear()
        //r.reset()
        //r.renderScene()

        // Position camera
        //val cam_node = ecs.getEntity(Scene.camera) as Node
        val cam_node = Scene.camera
        //val target_node = ecs.getEntity(sm) as Node
        val target_node = sm.node
        val prevCamPos = Vec3(cam_node.transform.pos)
        //cam_node.transform.pos.x = target_node.transform.pos.x
        //cam_node.transform.pos.y = target_node.transform.pos.y
        cam_node.transform.pos = target_node.transform.pos
        cam_node.transform.forceBuild()
        // Move camera back
        cam_node.transform.translateLocal(Vec3(0, 0, 100))
        //cam_node.transform.pos = Vec3(100f, 100f, cam_node.transform.pos.z)
        p("now pos:", cam_node.transform.pos, "prev pos:", prevCamPos)
        //println(cam_node.transform.pos)
        //println(prevCamPos)

        // Render
        r.setCamera(Scene.camera.camera!!)
        //r.renderMesh(ecs.getEntity(sm) as Node) // Fixme: use renderNode?
        r.renderMesh(sm.node) // Fixme: use renderNode?
        //r.renderScene()
        //drawGrid(10f, 1f)
        //r.finalize()

        // Copy active screen to dst tex
        //CopyTexture(sm.texR.tex, w, h)
        //TextureSys.copyTex(sm.rt().tex(), sm.texR)

        //r.renderRenderable(asRenderableI(sm.card))
        //sm.card.render(r)

        // Update card's tex
        //sm.card.tex!!.gl_tex = sm.rt().fb.tex
        //sm.texR.gl_tex = sm.rt().fb.tex
        //sm.card.tex = sm.texR

        //CopyTexture(sm.texR.gl_tex, w, h)
        //val ox = (vars.resX / 2) - w/2
        //val oy = (vars.resY / 2) - h/2
        val ox = (vars.resX / 2) - sm.dimX/2
        val oy = (vars.resY / 2) - sm.dimY/2

        //CopyTexture2(sm.texR.gl_tex, ox, oy, w, h)
        CopyTexture2(sm.texR.gl_tex, ox, oy, w, h)
        //sm.card.tex = sm.texR

        sm.rt().unbind()
        r.finalize()
        // XXX comment out to always update
        //sm.m_dirty = false
        //discard

        // Restore
        cam_node.transform.pos = prevCamPos
        cam_node.transform.forceBuild()

        sm.dirty = false
    }

    fun updateRect() {
        val t = sm.node.transform
            val projected = Scene.camera.camera!!.project(t.pos) // XXX slow?
            //val projected = Vec3(500, 400, 0)
            //p("pos:", t.pos)
            //p("projected:", projected)
            //p("projected:", vars.resY - projected.y, Renderer.getPw())
            val pjX = projected.x
            val pjY = projected.y
            //val pjY = vars.resY - projected.y
            val pX = (pjX - dimX / 2) * Renderer.getPw()
            val pY = (pjY - dimY / 2) * Renderer.getPh()
            //val pY = pjY * Renderer.getPh()
            //val pX = (projected.x - dimX/2) * Renderer.getPw()
            //val pY = (projected.y - dimY/2) * Renderer.getPh()
            //p(pX, pY)
            //sm.card.x = pX
            //sm.card.y = pY
            //val pX = (projected.x) * Renderer.getPw()
            sm.card.x = pX
            sm.card.y = pY
            //println(t.pos.toString())
            //p( t.pos.toString(), " ", t.rot.toString(), " ", t.customDirtyFlag)
            //p( t.pos, t.rot, t.customDirtyFlag)
    }

    fun update(dt: Float) {
        //echo "sm update"
        val t = sm.node.transform
        if (sm.dirty) {
            /*
            val projected = Scene.camera.project(t.pos)
            p("pos:", t.pos)
            p("projected:", projected)
            p("projected:", vars.resY - projected.y, Renderer.getPw())
            val pjX = projected.x
            val pjY = projected.y
            //val pjY = vars.resY - projected.y
            val pX = (pjX - dimX / 2) * Renderer.getPw()
            val pY = (pjY - dimY / 2) * Renderer.getPh()
            //val pY = pjY * Renderer.getPh()
            //val pX = (projected.x - dimX/2) * Renderer.getPw()
            //val pY = (projected.y - dimY/2) * Renderer.getPh()
            p(pX, pY)
            //sm.card.x = pX
            //sm.card.y = pY
            //val pX = (projected.x) * Renderer.getPw()
            sm.card.x = pX
            sm.card.y = pY
            //println(t.pos.toString())
            //p( t.pos.toString(), " ", t.rot.toString(), " ", t.customDirtyFlag)
            //p( t.pos, t.rot, t.customDirtyFlag)
            */
            updateRect()
        }

        //if t.isDirty():
        //  sm.m_dirty = true
        if (t.customDirtyFlag) {
            sm.dirty = true
            // Unset flag
            t.customDirtyFlag = false
            //discard
        }
    }
}