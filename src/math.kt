package ry

fun radians(x: Float): Float {
    return Math.toRadians(x.toDouble()).toFloat()
}
