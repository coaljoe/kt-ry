package tr

class Vehicle {
    var id: Int
    var info_max_speed: Int
    var accel: Float
    var speed: Float
    var weight: Int
    var engine: Engine?
    var parent: Vehicle?
    var child: Vehicle?
    // Is this vehicle controllable or dummy
    var has_controls: Boolean
    private val v = this

    init {
        id = 0
        info_max_speed = 100
        accel = 0.0f
        speed = 0.0f
        weight = 100
        engine = null
        parent = null
        child = null
        has_controls = false
    }
    
    fun add_engine() {
        v.engine = Engine()
    }

    fun has_engine(): Boolean {
        return v.engine != null
    }

    fun set_child(c: Vehicle) {
        if (c == v) {
            println("circular? $c == $v")
            println()
            throw Exception("can't be child of itself")
        }
        v.child = c
        c.parent = v
    }

    fun has_child(): Boolean {
        return v.child != null
    }

    fun has_parent(): Boolean {
        return v.parent != null
    }

    fun get_all_weight(): Int {
        return _get_all_weight(this)
    }

    private fun _get_all_weight(cur: Vehicle): Int {
        var weight_ = 0
        val max_children = 5
        var cur_child = 0
        println("cur: $cur, child: $cur.child, weight_: $weight_")
        if (cur.child != null) {
            cur_child += 1
            if (cur_child >= max_children) {
                error("max_children exceed")
            }
            weight_ +=  _get_all_weight(cur.child!!)
        }
        return cur.weight + weight_
    }

    fun update(dt: Float) {
        if (v.engine != null) {
            val engine = v.engine!!
            engine.update(dt)

            // a = f/m
            val mass = get_all_weight()
            val force = engine.power_n()
            v.accel = mass.toFloat() / force.toFloat()
            v.speed += v.accel * dt
        }
    }
}

// Component?
class TrainCar {
    var id: Int
    var vehicle: Vehicle
    var locomotive: Boolean
    private val tc = this

    init {
        id = 0
        vehicle = Vehicle()
        locomotive = false
    }

    fun join_child_car(c: TrainCar) {
        tc.vehicle.set_child(c.vehicle)
    }
}

// Abstract train?
// XXX: rename to TrainRecord?
class Train {
    //val cars: Weak
    val cars: MutableList<TrainCar>
    private val t = this

    init {
        cars = mutableListOf()
    }

    fun add_car(car: TrainCar): Boolean {
        println("adding car: $car")
        if (t.has_car(car)) {
            throw Exception("train already has this car")
            return false
        }
        t.cars.add(car)

        // Automatically join cars
        // Fixme?
        if (t.cars.size > 1) {
            //val last_car = t.cars.last()
            val last_car = t.cars.get(t.cars.lastIndex - 1)
            println("trying to join $last_car and $car")
            show_cars()
            last_car.join_child_car(car)
        }
        return true
    }

    fun has_car(car: TrainCar): Boolean {
        return t.cars.contains(car)
    }

    fun get_speed(): Float {
        if (t.cars.size < 1) {
            throw Exception("no cars")
        } else {
            return t.cars[0].vehicle.speed
        }
    }

    fun start() {

    }

    fun stop() {

    }

    fun show_cars() {
        println("show_cars:")
        for (car in t.cars) {
            println("-> car: $car")
        }
    }
}