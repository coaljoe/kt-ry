package tr

import glm.*
import glm.vec._3.Vec3

typealias PathPoint = Vec3

class RailTrackSegment {
    //var point_a: PathPoint
    //var point_b: PathPoint
    val points: MutableSet<PathPoint>
    //    private set
    var speed_limit: Int
    var electrified: Boolean
    var next_segment: RailTrackSegment?
    var prev_segment: RailTrackSegment?
    private val rts = this
    
    init {
        //point_a = Vec2()
        //point_b = Vec2()
        points = mutableSetOf()
        speed_limit = 60
        electrified = false
        next_segment = null
        prev_segment = null
        
        // Add segment to default rail track?
        RailTrack.add_segment(this)
    }
    
    private fun build() {
        RailTrack.join_segments()
    }
    
    fun add_points(vararg args: PathPoint) {
        for (arg in args) {
            rts.points.add(arg)
        }
        build()
    }    
    
    fun get_next_segment() {}
    fun get_prev_segment() {}
}

object RailTrack {
    val segments: MutableSet<RailTrackSegment>
    private val rt = this
    
    init {
        segments = mutableSetOf()
    }
    
    fun add_segment(s: RailTrackSegment) {
        rt.segments.add(s)
    }
    
    fun get_all_points(): MutableList<PathPoint> {
        val ret = mutableListOf<PathPoint>()
        for (seg in rt.segments) {
            ret += seg.points
        }
        return ret             
    }
    
    internal fun find_points_intersections(pt: PathPoint): MutableList<PathPoint> {
        val ret = mutableListOf<PathPoint>()
        // For all points
        for (pt2  in get_all_points()) {
            // Find the same point
            if (pt == pt2) {
                ret.add(pt2)
            }
        }
        return ret
    }
    
    internal fun join_segments() {
        println("RailTrack: join segments")
        for (pt in get_all_points()) {
            val pts = find_points_intersections(pt)
            println("-> pts: ${pts}")
        }
    }
}
