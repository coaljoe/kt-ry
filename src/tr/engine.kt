package tr

class Engine {
    // props:
    // Maximum power, kw
    var info_power: Int
    // state:
    var enabled: Boolean
    // Current power
    var power: Int
    private val e = this

    init {
        info_power = 4000
        enabled = false
        power = 0
    }

    fun enable() {
        e.enabled = true
    }

    fun disable() {
        e.enabled = false
    }
    
    // Get power in N m/s
    fun power_n(): Int {
        return e.power * 1000
    }

    fun update(dt: Float) {
        if (!e.enabled) {
            return
        }

        e.power = if (e.enabled) e.info_power * 1 else 0
    }
}