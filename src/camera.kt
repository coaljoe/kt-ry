package ry

import glm.mat.Mat4
import glm.vec._3.Vec3
import glm.vec._4.Vec4i
import glm.*

class Camera(node_: Node) {
	var node: Node
	var fov: Float
	var aspect: Float
	var znear: Float
	var zfar: Float
	var zoom: Float
	var ortho: Boolean
	var orthoPlanes: Array<Float>
	//var entity: ecs.Entity?
	val c = this
	init {
		node = node_
	    fov = 90f
		aspect = 1f
		znear = 0.1f
		zfar = 1000f
		zoom = 1.0f
		ortho = true
		orthoPlanes = arrayOf(0f, 0f, 0f, 0f)
		//entity = null
		c.setupView(45f, 1.0f, 0.1f, 1000f, true)
	}

	fun build() {
		println("Camera: build fov: ${c.fov} aspect: ${c.aspect} zoom: ${c.zoom}")
		assert(!c.aspect.isNaN())
		//assert(1==2)
		val ortho_scale = c.fov
		val aspect_ratio = c.aspect
		var xaspect = aspect_ratio
		var yaspect = 1.0f
		
		xaspect = xaspect * ortho_scale / (aspect_ratio * 2)
		yaspect = yaspect * ortho_scale / (aspect_ratio * 2)
		c.orthoPlanes = arrayOf<Float>(-xaspect * c.zoom, xaspect * c.zoom,
			-yaspect * c.zoom, yaspect * c.zoom)
		println("derp: ${c.orthoPlanes.asList()}")
		//p(c.orthoPlanes)
		//error(2)
	}

	fun setupView(fov: Float, aspect: Float, znear: Float, zfar: Float, ortho: Boolean) {
		println("Camera: setupView: ${fov} ${aspect} ${znear} ${zfar} ${ortho}")
		assert(!aspect.isNaN())
		c.fov = fov
		c.aspect = aspect
		c.znear = znear
		c.zfar = zfar
		c.ortho = ortho

		if (!c.ortho) {
			error("only ortho cams supported")
		}

		c.build()
	}

	fun getViewMatrix(): Mat4 {
		//println("ENTITY={$c.entity} ${c.entity == null}")
		//val tc = c.entity!!.get<Transform>()
		//val tc = ecs.getEntity(c).get<Transform>()
		val tc = c.node.transform
		//var m = Mat4x4(tc.mat)
		//var m = Mat4x4[float32](tc.mat).inverse
		//var m = Mat4x4(tc.mat)

		// XXX fixme: add non-inversible matrix check
		//echo tc.mat
		//echo inverse(tc.mat)
		//quit 2

		//var m = Mat4()(tc.mat)
		var m = Mat4(tc.mat)
		//var x = inverse(m)
		var x = m.inverse()
		//echo "herp"
		//println(tc.mat)
		//print_matrix(tc.mat)
		//print_matrix(Mat4())
		//println(Mat4().isIdentity())
		//print_matrix(x)
		return x
	}
	  
	fun getProjectionMatrix(): Mat4 {
		//let ratio = c.width / c.height
		//let ratio = c.aspect
		//ortho[float32](-ratio, ratio, -1.0, 1.0, c.znear, c.zfar)
		//ortho[float32](0.0, 1.0, 0.0, 1.0, c.znear, c.zfar)
		return glm.ortho(c.orthoPlanes[0], c.orthoPlanes[1],
				c.orthoPlanes[2], c.orthoPlanes[3], c.znear, c.zfar)
	}

	fun project(original: Vec3): Vec3 {
		//val tc = ecs.getEntity(c).get<Transform>()
		val tc = c.node.transform
		//val model = tc.mat
		val model = Mat4(tc.mat) * Mat4(c.getViewMatrix())
		//val model = c.getViewMatrix()
		//val proj = getProjectionMatrix()
		val proj = Mat4(getProjectionMatrix()) * Mat4(getViewMatrix())
		val viewport = Vec4i(0.0, 0.0, vars.resX, vars.resY);
		val projected = glm.project(original, model, proj, viewport)
		return projected
	}
}