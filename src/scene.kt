package ry

import glm.vec._3.Vec3
import glm.*

object Scene {
    private val nodes: LinkedHashMap<Int, Node> = linkedMapOf()
	// Active camera?
    //var camera: Camera
    var camera: Node
    val s = this        
    init {
    	println("[Scene] init $Scene")
        // Set Default scene
        /*
    	val cameraNode = newCameraNode()
    	s.addNode(cameraNode)
    	
        camera = cameraNode.get<Camera>()
        camera.entity = cameraNode
        */
        // Fixme
        //camera = Camera()
        camera = newCameraNode()
        //s.setDefaultScene()
    }

    fun init() {
    	println("Scene: init()")
    	s.setDefaultScene()
    }

    fun addNode(n: Node) {
        nodes[n.id] = n
    }

    fun getNodeById(id: Int): Node {
        return nodes[id]!!
    }

    fun hasNode(n: Node): Boolean {
        return n.id in nodes.keys
    }

    fun getNodes(): Array<Node> {
        return nodes.values.toTypedArray()
    }

    fun setDefaultScene() {
      println("[Scene] setDefaultScene()")
      //s.clear()
      
      // CameraNode
      val aspect: Float = vars.resX.toFloat() / vars.resY.toFloat()
      println("aspect: ${aspect}")
      assert(!aspect.isNaN())
      /*
      val cameraNode = newCameraNode()
      s.addNode(cameraNode)
      */
      val cameraNode = s.camera
      s.addNode(cameraNode)
      //error(2)
      //quit 2

      // Camera
      val cam = cameraNode.camera!!
      cam.setupView(45f, aspect, 0.1f, 1000f, true)
      //cam.entity = cameraNode
      println("$${cam}")
      //s.camera = cam
      //error(2)
      //camera = cameraNode

      // Set default camera position
      cameraNode.transform.pos = Vec3(0, 0, 10)
    
      // Light
      /*
      let l0 = newLightNode()
      s.addNode(l0)
      l0.get(Transform).pos = vec3f(5, 5, 5)
      l0.get(Light).intensity = 1.5
      s.light0 = l0
      */
	}
    
}

