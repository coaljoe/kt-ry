package ry

import java.util.Locale

class Ry {
    var app: App
    val scene: Scene
    val renderer: Renderer

    init {
        // Fixme?
        Locale.setDefault(Locale.US)

        app = App()
        //this.scene = Scene()
        //this.renderer = Renderer()
        this.scene = Scene
        this.renderer = Renderer

    }

    fun init(width: Int, height: Int) {
        println("ry init")
        vars.resX = width
        vars.resY = height

        app.init(width, height)
        scene.init()
        renderer.init(width, height)
        SpriteMeshSys.init()
    }

    fun render() {
        this.renderer.render()
    }

    fun step(): Boolean {
        val ret = app.step()
        val dt = app.dt

        SpriteMeshSys.update(dt)
        this.render()
        return ret
    }

    fun dt(): Float {
        return app.dt
    }
}