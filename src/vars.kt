package ry

object vars {
    var resX = 0
    var resY = 0
    // Conf. XXX: fixme?
    var confResPath = "res"
    val maxSpriteMeshSizeX = 8 // Read-only
    val maxSpriteMeshSizeY = 8 // Read-only
    val maxSpriteMeshSize = Pair(maxSpriteMeshSizeX, maxSpriteMeshSizeY)
}