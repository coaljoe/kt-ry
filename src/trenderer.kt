// Tile renderer.
package ry

class TRendererTile {
    var tex: Texture
    val card: Card
    val dirty: Boolean
    private val trt = this

    constructor(tsize: Int) {
        tex = Texture(tsize, tsize, false)
        card = Card()
        //card.is3d = true
        card.is3d = false
        card.setDim(0.5f, 0.5f, 0.5f, 0.5f)
        card.tex = tex
        //card.setDim(0f, 0f, 10f, 10f)
        dirty = true

        // Spawn
        Renderer.renderables.add(card)
    }
}

class TRenderer {
    var width: Int
    var height: Int
    val tsize: Int
    val tiles: MutableList<TRendererTile>
    val rt: RenderTarget
    val tileRt: RenderTarget
    val tcam: Node
    private val tr = this

    constructor(width: Int, height: Int) {
        this.width = width
        this.height = height
        tsize = 256
        tiles = mutableListOf()
        rt = RenderTarget("TRenderer RT", vars.resX, vars.resY, false)
        tileRt = RenderTarget("TRenderer tile RT", tsize, tsize, false)
        tcam = newCameraNode()

        // Spawn
        for (y in 0..this.height-1) {
            for (x in 0..this.width-1) {
                val t = TRendererTile(tsize)
                val pw = Renderer.getPw()
                val ph = Renderer.getPh()
                //t.card.setDim(x * Renderer.getPw(), y * Renderer.getPh(), 0.1f, 0.1f)
                t.card.setDim(x*tsize * pw, y*tsize * ph, tsize * pw, tsize * ph)
                tiles.add(t)
            }
        }
    }

    fun render(r: Renderer) {

        for (y in 0..this.height-1) {
            for (x in 0..this.width - 1) {
                val tile = tr.tiles[y*height+x]
                //tile.tex.gl_tex = tr.rt.tex()
                r.finalize()
                rt.bind()
                r.clear()
                r.setCamera(tcam.camera!!)
                r.renderScene()
                //CopyTexture2(tile.tex.gl_tex, 0, 0, tr.tsize, tr.tsize)
                tile.tex.gl_tex = tr.rt.tex()
                rt.unbind()
                r.finalize()
            }
        }
    }
}