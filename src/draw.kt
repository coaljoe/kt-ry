package ry

import org.lwjgl.opengl.GL11.*

fun drawLine(x1: Float, y1: Float, z1: Float, x2: Float, y2: Float, z2: Float) {
    glBegin(GL_LINES)
    glVertex3f(x1, y1, z1)
    glVertex3f(x2, y2, z2)
    glEnd()
}

fun drawGrid(size: Float, cell_size: Float) {
    glLineWidth(1.0f)
    val n = size
    val s = cell_size
    glBegin(GL_LINES)
    var i = 0.0f
    while (i < n) {
        if (i == 0.0f) {
            glColor3f(0.3f, 0.6f, 0.3f)
        } else {
 
            glColor3f(0.25f, 0.25f, 0.25f)
        }
        glVertex3f(i, 0f, 0f)
        glVertex3f(i, n, 0f)
        if (i == 0f) {
            glColor3f(0.6f, 0.3f, 0.3f)
        } else {
            glColor3f(0.25f, 0.25f, 0.25f)
        }
        glVertex3f(0f, i, 0f)
        glVertex3f(n, i, 0f)
        i += s
    }
    glEnd()
}

// Draw 2D Rect (unfilled)
fun drawRect(x: Float, y: Float, w: Float, h: Float) {
    /*
        w := x2-x1
        h := y2-y1
        DrawLine(x1, y1, 0, x1, w, 0)
        DrawLine(x2, y1, 0, x2, -h, 0)
        DrawLine(x2, y2, 0, x1, y2, 0)
        DrawLine(x2, y2, 0, x1, y1, 0)
    */
    // https://en.wikibooks.org/wiki/
    // OpenGL_Programming/Modern_OpenGL_Tutorial_2D
    //gl.PushMatrix()
    //gl.MatrixMode(gl.MODELVIEW)
    //gl.LoadIdentity()
    //gl.Translatef(0.375, 0.375, 0)
    drawLine(x, y, 0f, x+w, y, 0f)     // bottom
    drawLine(x+w, y, 0f, x+w, y+h, 0f) // right
    drawLine(x, y+h, 0f, x+w, y+h, 0f) // top
    drawLine(x, y, 0f, x, y+h, 0f)     // left
    //gl.PopMatrix()
    /*
        x1, y1 := float32(x), float32(y)
        x2, y2 := float32(w), float32(h)
        gl.PolygonMode(gl.FRONT_AND_BACK, gl.LINE)
        gl.Rectf(x1, y1, x2, y2)
        gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
    */
    /*
        // Version with fixed 'missing pixel' effect,
        // seems not perfect but better.
        // factor-language.blogspot.com/2008/11/some-recent-ui-rendering-fixes.html
        pw := rxi.Renderer().GetPw()
        ph := rxi.Renderer().GetPh()
        o1x, o1y := 0.5*pw, 0.5*ph               // top-left offset
        o2x, o2y := -0.3*pw, -0.3*ph             // bottom-right offset
        DrawLine(x, y, 0, x+w+o2x, y+o2y, 0)     // bottom
        DrawLine(x+w+o2x, y+o2y, 0, x+w, y+h, 0) // right
        DrawLine(x+o1x, y+h+o1y, 0, x+w, y+h, 0) // top
        DrawLine(x+o1x, y+o1y, 0, x, y+h, 0)     // left
    */
}

fun drawSetColor(r: Float, g: Float, b: Float) {
    glColor3f(r, g, b)
}