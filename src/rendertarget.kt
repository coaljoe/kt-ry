package ry

class RenderTarget {
    var name: String
    var depth: Boolean
    val width: Int
    val height: Int
    val fb: Framebuffer
    private val rt = this

    fun tex(): Int = rt.fb.tex

    constructor(name: String, width: Int, height: Int, depth: Boolean) {
        this.name = name
        this.width = width
        this.height = height
        this.depth = depth
        fb = Framebuffer(width, height, depth)
        //rt.create()
        glcheck()

        // register element
        //_RenderTargetSys.Add(rt)
    }

    fun bind() {
        rt.fb.bind()
    }

    fun unbind() {
        rt.fb.unbind()
    }
}

