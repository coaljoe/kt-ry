package ry

object SpriteMeshSys {
    val elems: MutableList<SpriteMesh>
    var rt: RenderTarget?
    private val s = this

    init {
        println("[SpriteMeshSys] new")
        elems = mutableListOf()
        rt = null
        //s.elems = newOrderedTable[int, Node]()
        //println("s.elems", s.elems)
        //gSpriteMeshSys = s
    }

    fun init() {
        println("[SpriteMeshSys] init")
        //var w = vars.resX-300
        //var h = vars.resY
        var w = 512
        var h = 512
        //#let(w, h) = vars.maxSpriteMeshSize
        rt = RenderTarget("SpriteMeshRT", w, h, false)
    }

    fun start() {}

    fun add(el: SpriteMesh) {
        if (s.elems.contains(el)) {
            error("already have element")
        }
        s.elems.add(el)
    }

    fun update(dt: Float) {
        for (el in s.elems) {
            el.update(dt)
        }
    }

}
