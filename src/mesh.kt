package ry

// Component
class Mesh(node_: Node) {
	var node: Node
	var meshName: String
	var material: Material?
	var mb: MeshBuffer
	private val m = this
	
	init {
		node = node_
		meshName = ""
	    material = null
		mb = MeshBuffer()
	}

	fun loadFromMeshBuffer(mb: MeshBuffer) {
		m.mb = mb
	}
}