package ry

import org.lwjgl.*
import org.lwjgl.glfw.*
import org.lwjgl.opengl.*
import org.lwjgl.system.*
import java.nio.*
import java.time.Clock.system
import org.lwjgl.glfw.Callbacks.*
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.opengl.GL11.*
import org.lwjgl.system.MemoryStack.*
import org.lwjgl.system.MemoryUtil.*
import org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose
import org.lwjgl.glfw.GLFW.GLFW_RELEASE
import org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE
import java.awt.SystemColor.window
import org.lwjgl.glfw.GLFW.glfwSetKeyCallback
import glm.vec._3.Vec3


class Window {
    var glfwWin: Long = 0

    fun init(width: Int, height: Int) {
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        GLFWErrorCallback.createPrint(System.err).set()

        // Initialize GLFW
        if (!glfwInit())
            throw IllegalStateException("Unable to initialize GLFW")

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2)
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1)

        glfwWindowHint(GLFW_RESIZABLE, GL_FALSE)
        glfwWindowHint(GLFW_FOCUSED, GL_TRUE)

        glfwWindowHint(GLFW_RED_BITS, 8)
        glfwWindowHint(GLFW_GREEN_BITS, 8)
        glfwWindowHint(GLFW_BLUE_BITS, 8)
        glfwWindowHint(GLFW_ALPHA_BITS, 0)
        glfwWindowHint(GLFW_DEPTH_BITS, 24)
        glfwWindowHint(GLFW_STENCIL_BITS, 0)

        glfwWin = glfwCreateWindow(width, height, "ry", NULL, NULL)
        if (glfwWin == NULL)
            throw RuntimeException("Failed to create the GLFW window")

        fun getCameraNode(): Node {
            //return (ecs.getEntity(Scene.camera) as Node)
            return Scene.camera
        }

        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(glfwWin) { window, key, scancode, action, mods ->
            if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
                glfwSetWindowShouldClose(window, true) // We will detect this in the rendering loop
            if (key == GLFW_KEY_F12 && action == GLFW_RELEASE)
                TextureSys.dumpTextures("tmp")
            /*
            if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
                getCameraNode().transform.pos.x -= 0.2f
                getCameraNode().transform.forceBuild()
            }
            if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
                getCameraNode().transform.pos.x += 0.2f
                getCameraNode().transform.forceBuild()
            }
            if (key == GLFW_KEY_UP && action == GLFW_PRESS) {
                getCameraNode().transform.pos.y += 0.2f
                getCameraNode().transform.forceBuild()
            }
            if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
                getCameraNode().transform.pos.y -= 0.2f
                getCameraNode().transform.forceBuild()
            }
            */
        }

        glfwMakeContextCurrent(glfwWin)

        //if not gladLoadGL(getProcAddress):
        //  quit "Error initialising OpenGL"

        glfwSwapInterval(1)

        // init opengl
        //loadExtensions()
        GL.createCapabilities()

        glfwShowWindow(glfwWin)
    }

    fun hasExit(): Boolean {
        return glfwWindowShouldClose(glfwWin)
    }

    fun update(dt: Float) {
        glfwSwapBuffers(glfwWin)
        glfwPollEvents()
    }
}

class App {
    var win: Window = Window()
    var lastTime: Long = 0
    var dt: Float = 0f
    var frame: Int = 0

    fun init(width: Int, height: Int) {
        win.init(width, height)
    }

    fun quit() {
        println("app.quit")
    }

    fun update(dt: Float) {
        // Pan camera
        //val cameraNode = ecs.getEntity(Scene.camera) as Node
        val cameraNode = Scene.camera
        var adjX = 0.0f
        var adjY = 0.0f
        var speed = 5.0f
        var cameraMoved = false
        if (glfwGetKey(win.glfwWin, GLFW_KEY_LEFT_SHIFT) or
                glfwGetKey(win.glfwWin, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS) {
            speed *= 4
            cameraMoved = true
        }
        if (glfwGetKey(win.glfwWin, GLFW_KEY_LEFT) == GLFW_PRESS) {
            adjX = -speed
            cameraMoved = true
        } else if (glfwGetKey(win.glfwWin, GLFW_KEY_RIGHT) == GLFW_PRESS) {
            adjX = speed
            cameraMoved = true
        }
        if (glfwGetKey(win.glfwWin, GLFW_KEY_UP) == GLFW_PRESS) {
            adjY = speed
            cameraMoved = true
        } else if (glfwGetKey(win.glfwWin, GLFW_KEY_DOWN) == GLFW_PRESS) {
            adjY = -speed
            cameraMoved = true
        }
        //cameraNode.transform.pos += Vec3(adjX * dt, adjY * dt, 0)
        cameraNode.transform.translateLocal(Vec3(adjX * dt, adjY * dt, 0))
        cameraNode.transform.forceBuild()

        if (cameraMoved) {
            for (sm in SpriteMeshSys.elems.toTypedArray()) {
                sm.onCameraMove(adjX, adjY)
            }
        }

        win.update(dt)
    }

    fun step(): Boolean {
        if (win.hasExit()) {
            quit()
            return false
        }

        val thisTime = System.nanoTime()

        // first frame
        if (lastTime == 0L) {
            lastTime = thisTime
        }

        dt = (thisTime - lastTime) / 1E9f
        //echo timeNow, " ", a.timeLast, " ", a.dt
        lastTime = thisTime

        //ryi().render()

        update(dt)
        frame++

        return true
    }
}