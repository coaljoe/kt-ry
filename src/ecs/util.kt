package ecs

// Get entity for component
fun getEntity(c: Any): Entity {
	val en = defaultWorld.componentEntityMap.get(c)
	if (en != null) {
		return en
	} else {
		error("entity not found for component: $c")
	}
}