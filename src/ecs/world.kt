package ecs

internal val defaultWorld = World()

class World {
	private val entities: MutableList<Entity>
	internal val componentEntityMap: MutableMap<Any, Entity>
	val w = this
	init {
		entities = mutableListOf()
		componentEntityMap = mutableMapOf()
	}

	fun createEntity(): Entity {
		val en = Entity()
		return en
	}

	fun addEntity(en: Entity) {
		w.entities.add(en)
	}
}