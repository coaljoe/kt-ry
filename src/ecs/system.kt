package ecs

open class System<T> {
    val elems: MutableList<T>

    init {
        elems = mutableListOf()
    }

    fun addElem(el: T) {
        elems.add(el)
    }
}