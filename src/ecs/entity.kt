package ecs

private var maxEntityId = 0

open class Entity {
    val id: Int
    val components: MutableList<Any>

    
    init {
        //println("Entity.init")
        this.id = maxEntityId
        this.components = mutableListOf<Any>()
        maxEntityId++

        defaultWorld.addEntity(this)
    }

    inline fun <reified T: Any> get(): T {
        for (c in this.components) {
            if (c is T) {
                return c
            }
        }
        printComponents()
        error("Ecs: Component not found")
    }
    
    inline fun <reified T: Any> has(): Boolean {
        for (c in this.components) {
            if (c is T) {
                return true
            }
        }
        return false
    }

    fun addComponent(c: Any) {
        if (components.contains(c)) {
            printComponents()
            error("Ecs: Entity already has this component")
        }
        this.components.add(c)

        defaultWorld.componentEntityMap[c] = this
    }

    fun printComponents() {
        println("Ecs: Entity.printComponents:")
        println(components)
        /*
        for (c in components) {
            println("
        }
        */
    }
}