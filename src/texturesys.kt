package ry

import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL12.*
import org.lwjgl.opengl.GL13.*
import org.lwjgl.opengl.GL14.*
import org.lwjgl.opengl.GL15.*
import org.lwjgl.opengl.GL20.*
import org.lwjgl.opengl.GL21.*
import org.lwjgl.opengl.GL30.*

object TextureSys: GameSystem<Texture>() {
    var readFbo: Framebuffer?
    var writeFbo: Framebuffer?
    private val s = this

    init {
        readFbo = null
        writeFbo = null
    }

    // Init system
    fun init() {
        s.readFbo = Framebuffer(vars.resX, vars.resY, false)
        s.writeFbo = Framebuffer(vars.resX, vars.resY, false)
    }

    fun copyTexture(src: Texture, dst: Texture) {
        //Blit FBO to normal frame buffer
        glBindFramebuffer(GL_READ_FRAMEBUFFER, s.writeFbo!!.gl_fbo)
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0)
        glBlitFramebuffer(0, 0, vars.resX, vars.resY, 0, 0,
                vars.resY, vars.resY, GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT, GL_NEAREST)
        glBindFramebuffer(GL_READ_FRAMEBUFFER, 0)
        glcheck()
    }

    fun dumpTextures(path: String) {
        println("TextureSys: dumpTextures; path: $path")
        for ((i, t) in s.elems.withIndex()) {
            t.save("$path/ry__dump_tex_%02d.png".format(i))
        }
        println("done")
        //error(2)
    }
}
