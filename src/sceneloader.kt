package ry

import java.io.*
import java.nio.file.*

class SceneLoader {
    //loader: ColladaLoader
    val loader: ObjLoader
    val nodes: MutableList<Node>
    private val sl = this
    
    init {
        loader = ObjLoader()
        nodes = mutableListOf()
    }
	
	fun load(path: String): MutableList<Node> {
	    println("[SceneLoader] loading path: ${path}")
		
		if (!File(path).exists()) {
		    error("path not found; path: ${path}")
		}
	  
	    sl.loader.load(path)
	    val positionsRaw = sl.loader.toRaw(sl.loader.out_positions)
	    val normalsRaw = byteArrayOf() //sl.out_normals
	    val texcoordsRaw = byteArrayOf() //sl.out_texcoords
	    
	    val p = sl.loader.out_positions.toTypedArray().toFloatArray()
	    println("p: ${p.asList()}")
	    val positions = p
	    //val positions = floatArrayOf()
	    val normals = floatArrayOf()
	    val texcoords = floatArrayOf()
	    
        val n = newMeshNode()
        val m = n.mesh!!
        val mb = MeshBuffer()
         // Create meshbuffer from raw data
        //mb.addRawData(positionsRaw, normalsRaw, texcoordsRaw)
        mb.addData(positions, normals, texcoords)
        m.loadFromMeshBuffer(mb)
	
        sl.nodes.add(n)
		
        println("[SceneLoader] loading done")
        return sl.nodes	
    }
}