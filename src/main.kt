package ry

import glm.*
import glm.vec._3.Vec3
import tr.*

fun main(args: Array<String>) {
    println("main()")

    val ry = Ry()
    ry.init(960, 540)

    val n = newEmptyNode()
    println(n)

    //val c1 = Card()
    //c1.setDim(0f, 0f, 0.5f, 0.5f)
    //ry.renderer.renderables.add(c1)
    //ry.renderer.renderables.add(asRenderableI(c1))

    //val ol = ObjLoader()
    //ol.load("/home/j/dev/lib/nim/ry/tmp/dragon.obj")

    val sl = SceneLoader()
    //val node = sl.load("/home/j/dev/lib/nim/ry/res/primitives/square_textured/square_textured.obj")[0]
    //val node = sl.load("/home/j/dev/lib/nim/ry/res/primitives/box_textured/box_textured.obj")[0]
    val node = sl.load("../../nim/ry/res/primitives/box_textured/box_textured.obj")[0]
    Scene.addNode(node)

    val m = node.mesh!!
    println("numPositions: ${m.mb.numPositions}")
    //error(2)

    // Tr test

    println("Tr test:")

    val t = Train()
    val tc1 = TrainCar()
    val tc2 = TrainCar()
    t.add_car(tc1)
    t.add_car(tc2)
    //t.add_car(tc2)
    //tc1.join_child_car(tc2)

    println("speed: ${tc1.vehicle.has_child()}")
    println("all weight: ${tc1.vehicle.get_all_weight()}")
    try {
        println("speed: ${t.get_speed()}")
    } catch (e: Exception) {
        println("ERR: ${e.message}")
    }

    println()
    
    // Acceleration test
    tc1.vehicle.add_engine()
    tc1.vehicle.engine!!.enable()
    println("speed: ${tc1.vehicle.speed}")
    
    tc1.vehicle.update(0.01f)
    println("speed: ${tc1.vehicle.speed}")
    println("train speed: ${t.get_speed()}")

    
    // Rail track test
    println("rail track test:")
    //val rt1 = RailTrack()
    val rts1 = RailTrackSegment()
    rts1.electrified = true
    rts1.add_points(PathPoint(0, 0, 0), PathPoint(1, 0, 0))
    val rts2 = RailTrackSegment()
    rts2.electrified = false
    rts2.speed_limit = 20
    rts2.add_points(PathPoint(1, 0, 0), PathPoint(2, 0, 0), PathPoint(3, 0, 0))
    
    //error(2)


    val sm_node = newSpriteMeshNode()
    val sm = sm_node.spriteMesh!!
    sm.load("../../nim/ry/res/primitives/box_textured/box_textured.obj")
    sm_node.transform.pos = Vec3(2f, 2f, 0f)
    Scene.addNode(sm_node)


    // Order is important (Fixme?)
    //ry.renderer.renderables.add(sm.card)
    //ry.renderer.renderables.add(sm)

    //if (true) {
        val sm_node2 = newSpriteMeshNode()
        val sm2 = sm_node2.spriteMesh!!
        sm2.load("../../nim/ry/res/primitives/box_textured/box_textured.obj")
        sm_node2.transform.pos = Vec3(6f, 6f, 0f)
        sm_node2.transform.rot = Vec3(0, 0, 45)
        val px = 4 * Renderer.getPw()
        val py = 4 * Renderer.getPh()
        //sm2.card.setDim(0.05f, 0.05f, 0.5f / Renderer.getAspect(), 0.5f) // * Renderer.getAspect())
        //sm2.card.setDim(0.05f, 0.05f, 0.5f, 0.5f * Renderer.getAspect()) // * Renderer.getAspect())
        Scene.addNode(sm_node2)

        // Order is important (Fixme?)
        //ry.renderer.renderables.add(sm2.card)
        //ry.renderer.renderables.add(sm2)
    //}

    //ry.renderer.renderables.add(sm)
    //ry.renderer.renderables.add(sm2)


    if (true) {
        ry.renderer.renderables.add(sm.card)
        ry.renderer.renderables.add(sm2.card)
        ry.renderer.renderables.add(sm)
        ry.renderer.renderables.add(sm2)
    }


/*
    val cam_node = ecs.getEntity(Scene.camera) as Node
    val target_node = sm_node2
    cam_node.transform.pos.x = target_node.transform.pos.x
    cam_node.transform.pos.y = target_node.transform.pos.y
    cam_node.transform.forceBuild()
    //cam_node.transform.pos.x = 100f
    //cam_node.transform.pos.y = 100f
    cam_node.transform.pos = Vec3(10f, 13f, cam_node.transform.pos.z)
*/

    val cards = mutableListOf<Card>()
    val sms = mutableListOf<SpriteMesh>()
    if (true) {
        val mul = 1
        val w = 16 * mul
        val h = 16 * mul
        for (y in 0..h-1) {
            for (x in 0..w-1) {
                val sm_node = newSpriteMeshNode()
                val sm = sm_node.spriteMesh!!
                //sm.load("../../nim/ry/res/primitives/box_textured/box_textured.obj")
                sm.load("res/models/tree/tree.obj")
                sm_node.transform.pos = Vec3(x.toFloat(), y.toFloat(), 0f)
                sm_node.transform.scale = Vec3(0.2, 0.2, 0.2)
                //Scene.addNode(sm_node)
                cards.add(sm.card)
                sms.add(sm)
            }
        }
        ry.renderer.renderables.addAll(cards)
        ry.renderer.renderables.addAll(sms)
    }

    // Tilt camera
    //val tc = ecs.getEntity(Scene.camera).get<Transform>()
    val tc = Scene.camera.transform
    //ecs.getEntity(Scene.camera).get<Transform>().rot = Vec3(50, 0, 45)
    // Flipped-z coords
    tc.rot = Vec3(-50, 180, 180+-45)
    tc.translateLocal(Vec3(0, 0, 10))
    //ecs.getEntity(Scene.camera).get<Transform>().rot = Vec3(60, 0, 45)
    //ecs.getEntity(Scene.camera).get<Transform>().rot = Vec3(75, 0, 45)
    //ecs.getEntity(Scene.camera).get<Transform>().rot = Vec3(0, 0, -45)
    //ecs.getEntity(Scene.camera).get<Transform>().rot = Vec3(0, 0, 90)

    //println(Scene.camera.camera!!.aspect)
    //error(2)


    var rotSpeed = 20f
    var rotate = true
    while (ry.step()) {
        val dt = ry.dt()
        //println("dt = $dt")
        if (rotate) {
            //val tc = node.get<Transform>()
            val tc = node.transform
            val oldRot = tc.rot
            tc.rot = oldRot + Vec3(rotSpeed * dt, rotSpeed * dt, rotSpeed * dt)
        }

        //sm.update(dt)
    }

    println("done")
}