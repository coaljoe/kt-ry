package ry

class Card : RenderableI {
    var x = 0f
    var y = 0f
    var w = 0f
    var h = 0f
    var z = 0f
    var tex: ry.Texture? = null
    var showBorder: Boolean
    var is3d: Boolean
    private val c = this

    init {
        z = 1f
        showBorder = true
        is3d = false
    }

    fun setDim(x: Float, y: Float, w: Float, h: Float) {
        this.x = x
        this.y = y
        this.w = w
        this.h = h
    }

    override fun render(r: Renderer) {
        //println("card render")
        if (tex != null) {
           tex!!.bind()
        }

        r.enableBlending()
        r.renderQuad(c.x, c.y, c.w, c.h, c.z)
        r.disableBlending()

        if (tex != null) {
            tex!!.unbind()
        }

        if (c.showBorder) {
            if (!c.is3d) {
                r.set2DMode()
            }
            //drawSetColor(0.5f, 1.0f, 0.5f)
            drawSetColor(0.6f, 0.1f, 0.1f)
            drawRect(c.x, c.y, c.w, c.h)
            if (!c.is3d) {
                r.unset2DMode()
            }
        }
    }
}