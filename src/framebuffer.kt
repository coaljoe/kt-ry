// An abstraction over the opengl framebuffer object (FBO).
package ry

//import org.lwjgl.opengles.GLES20.*
import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL12.*
import org.lwjgl.opengl.GL13.*
import org.lwjgl.opengl.GL14.*
import org.lwjgl.opengl.GL15.*
import org.lwjgl.opengl.GL20.*
import org.lwjgl.opengl.GL21.*
import org.lwjgl.opengl.GL30.*
import org.lwjgl.opengl.*
import org.lwjgl.opengl.GL.*
//import org.lwjgl.opengl

class Framebuffer {
    var width: Int
    var height: Int
    // Is a depth frame buffer
    var modeDepth: Boolean
    // Always false
    var onlyColor: Boolean
    var tex: Int
    //gl:
    var gl_fbo: Int
    // Depth render buffer object
    var gl_rbo_depth: Int
    private val f = this

    init {
        width = 0
        height = 0
        modeDepth = false
        onlyColor = false
        tex = 0
        gl_fbo = 0
        gl_rbo_depth = 0

        //create()
    }

    constructor(width: Int, height: Int, modeDepth: Boolean) {
        f.tex = 0
        f.width = width
        f.height = height
        f.modeDepth = modeDepth
        f.onlyColor = false
        create()
    }

    private fun create() {
        //f.tex = CreateTexture(f.width, f.height, f.modeDepth)
        f.tex = createTex(f.width, f.height, f.modeDepth)
        glcheck()

        // tune the texture
        if (f.modeDepth) {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        } else {
            // need for fxaa
            //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        }

        // create FBO
        f.gl_fbo = glGenFramebuffers()
        //rt.fbo.Bind()
        glBindFramebuffer(GL_FRAMEBUFFER, f.gl_fbo)
        glcheck()

        if (f.onlyColor) {
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                    GL_TEXTURE_2D, f.tex, 0)
            glcheck()
        } else {

            // depth buffer
            f.gl_rbo_depth = glGenRenderbuffers()
            //rbo_depth.Bind()
            glBindRenderbuffer(GL_RENDERBUFFER, f.gl_rbo_depth)
            glcheck()
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24,
                    f.width, f.height)
            glcheck()
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                    GL_RENDERBUFFER, f.gl_rbo_depth)
            glcheck()
        }

        // attach the texture
        if (!f.modeDepth) {
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                    GL_TEXTURE_2D, f.tex, 0)
        } else {
            glDrawBuffer(GL_NONE)
            glReadBuffer(GL_NONE)
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                    GL_TEXTURE_2D, f.tex, 0)
        }
        glcheck()

        // check status
        val status = glCheckFramebufferStatus(GL_FRAMEBUFFER)
        if (status != GL_FRAMEBUFFER_COMPLETE) {
            error("Bad FBO, status: %d".format(status))
        }

        glcheck()

        // unbind
        //rt.fbo.Unbind()
        //rbo_depth.Unbind()
        glBindFramebuffer(GL_FRAMEBUFFER, 0)
        glcheck()
    }

    fun bind() {
        glBindFramebuffer(GL_FRAMEBUFFER, f.gl_fbo)
    }

    fun unbind() {
        glBindFramebuffer(GL_FRAMEBUFFER, 0)
    }

}