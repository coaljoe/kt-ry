package ry

import glm.mat.Mat4
import glm.mat.Mat3
import glm.vec._3.Vec3
import glm.*

class Transform {
    var mat: Mat4 = Mat4()
        private set
    // Fixme: XXX setters are slow and buggy
    // rebuild mat in the getter?
    var pos: Vec3 = Vec3(0.0f)
        set(value) { field = value; build() }
    var rot: Vec3 = Vec3(0.0f)
        set(value) { field = value; build() }
    var scale: Vec3 = Vec3(1.0f)
        set(value) { field = value; build() }
    //var dirty: Boolean = true
    var customDirtyFlag: Boolean = false
    private val t = this


    init {
        build()
    }

    private fun build() {
        val tm = Mat4().translate(pos)
        var rm = Mat4()
        rm = rm.rotate(radians(rot.z), Vec3(0.0f, 0.0f, 1.0f))
        rm = rm.rotate(radians(rot.y), Vec3(0.0f, 1.0f, 0.0f))
        rm = rm.rotate(radians(rot.x), Vec3(1.0f, 0.0f, 0.0f))
        val sm = Mat4().scale(scale)
        t.mat = tm * rm * sm
        //dirty = false
        t.customDirtyFlag = true
    }

    fun forceBuild() {
        build()
    }

    // Move by vec (locally)
    fun translateLocal(v: Vec3) {
        val mat = t.mat.toMat3() // world(?) rotation matrix
        //mat.SetTranslation(0, 0, 0) // clear translation

        val trans_local = v
        val trans_world = mat * trans_local
        val new_pos = t.pos + trans_world

        t.pos = new_pos
    }

    /*
    fun mat(): Mat4 {
        mat
    }
    */
}