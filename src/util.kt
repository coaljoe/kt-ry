package ry

import java.nio.FloatBuffer

import org.lwjgl.glfw.Callbacks.*
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL12.*
import org.lwjgl.opengl.GL14.*
//import org.lwjgl.opengl.GL30.*
import org.lwjgl.system.MemoryStack.*
import org.lwjgl.system.MemoryUtil.*

import glm.mat.Mat4
import glm.*
import glm.vec._3.Vec3

// Debug

fun p(vararg args: Any) {
	var s = ""
	var i = 0
	for (arg in args) {
		//println("class: ${arg::class}")
		//println("arg: ${arg}")

		if (arg is Vec3) {
			//println("is Vec3")
			val ss = "Vec3(%.5f, %.5f, %.5f)".format(arg.x, arg.y, arg.z)
			//println(ss)
			s += "%s".format(ss)
		} else {
			s += "${arg}"
		}
		if (i != args.size - 1) {
			s += ", "
		}
		i++
	}

	println("[$s]")
	//println("[%s]".format(args.asList()))
}

fun pp(vararg args: Any) {
    p(args)
	error(2)
}

// GL

fun xglMultMatrix(m: Mat4) {
	/*
	// Doesn't work properly
	var buf = FloatBuffer.allocate(16)
	//buf = m.to(buf)
	m.to(buf)
	//buf.rewind()
	//buf.flip()
	//buf.rewind()
	print_matrix(m)
	println("buf: %s".format(buf.array().asList()))
	glMultMatrixf(buf)

	var vv = FloatArray(16)
	for (i in 0..15) {
		vv[i] = Math.random().toFloat()
	}

	glMultMatrixf(vv)
	println("vv: %s".format(vv.asList()))
	*/

	
	// Create array by hand
	val vv = floatArrayOf(m.a0, m.a1, m.a2, m.a3, m.b0, m.b1, m.b2, m.b3, m.c0, m.c1, m.c2, m.c3, m.d0, m.d1, m.d2, m.d3)

	glMultMatrixf(vv)
	//println("vv: %s".format(vv.asList()))


	/*
	// XXX Doesn't work as expected?
	println("attempt to read matrix...")
	var v = FloatArray(16)
	var lbuf = FloatBuffer.allocate(16)
	glGetFloatv(GL_MODELVIEW_MATRIX, v)
	println("lbuf: %s".format(lbuf.array().asList()))
	println("v: %s".format(v.asList()))
	*/
}

fun glcheck() {
	val err = glGetError()
	if (err != GL_NO_ERROR) {
		error("glcheck fail")
	}
}

fun createTex(width: Int, height: Int, depth: Boolean): Int {
	var tex = glGenTextures()
	//tex.Bind(GL_TEXTURE_2D)
	glBindTexture(GL_TEXTURE_2D, tex)

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)

	if (!depth) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8,
				width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL)
	} else {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24,
				width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL)
	}

	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL)
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE)

	//tex.Unbind(GL_TEXTURE_2D)
	glBindTexture(GL_TEXTURE_2D, 0)

	return tex
}

// GLM

fun print_matrix(m: Mat4) {
    println("Mat4:")
	println("%f %f %f %f".format(m.a0, m.b0, m.c0, m.d0))
	println("%f %f %f %f".format(m.a1, m.b1, m.c1, m.d1))
	println("%f %f %f %f".format(m.a2, m.b2, m.c2, m.d2))
	println("%f %f %f %f".format(m.a3, m.b3, m.c3, m.d3))
}
