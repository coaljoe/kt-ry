package ry
//import ry

import glm.vec._3.Vec3
import org.junit.Test as test
import org.junit.Assert.*

class Tests {
    @test fun simple() {
        assertEquals(4, 2 * 2)
    }

    @test fun project() {
        val ry = Ry()
        ry.init(960, 540)
        val cn = newCameraNode()
        val c = cn.get<Camera>()
        p(c.project(Vec3(0,0,0)))
        p(c.project(Vec3(10,0,0)))
    }
}